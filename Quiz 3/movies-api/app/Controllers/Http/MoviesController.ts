import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateMovieValidator from 'App/Validators/CreateMovieValidator'
// import UpdateMovieValidator from 'App/Validators/UpdateMovieValidator'

export default class MoviesController {
  public async index({request,response}: HttpContextContract) {
    if (request.qs().title){
      let title = request.qs().title
      let movieFiltered = await Database.from('movies').join('genres','movies.genre_id','genres.id').select('movies.id','title','resume','release_date','genres.name as genre').where('title','like','%'+title+'%')
      return response.status(200).json({message:'success get movie',data:movieFiltered})
    }
    let movies =  await Database.from('movies').join('genres','movies.genre_id','genres.id').select('movies.id','title','resume','release_date','genres.name as genre')

    response.status(200).json({message:'success get movie',data:movies})
  }

  public async create({}: HttpContextContract) {
    
  }

  public async store({request,response}: HttpContextContract) {
    console.log("s")
    try{
      await request.validate(CreateMovieValidator);
      console.log(request.body())
      let genres = await Database.from('genres').where('id',request.input('genre_id')).select('*')
      if(genres.length<1){
        return response.badRequest({message:'genre tidak ada'})
      }
      let newMovieId = await Database.table('movies').returning('id').insert({
        title:request.input('title'),
        resume:request.input('resume'),
        release_date:request.input('release_date'),
        genre_id:request.input('genre_id')
      })
      console.log("3")
      response.created({status:"success add movie", newId:newMovieId[0]})
      console.log("4")
    }
    catch(error){
        response.badRequest(error.messages)
        console.log("5")
    }
  }

  public async show({response,params}: HttpContextContract) {
    let movies = await Database.from('movies').join('genres','movies.genre_id','genres.id').where('movies.id',params.id).select('movies.id','title','resume','release_date','genres.name as genre').firstOrFail()
        response.ok({status:"success get data", data:movies})
        console.log("s")
  }

  public async edit({}: HttpContextContract) {}

  public async update({response,params,request}: HttpContextContract) {
    // await request.validate(UpdateMovieValidator);
    let genres = await Database.from('genres').where('id',request.input('genre_id')).select('*')
      if(genres.length<1){
        return response.badRequest({message:'genre tidak ada'})
      }
    let id = params.id
        let affectedRows = await Database.from('movies').where('id',id).update({
          title:request.input('title'),
          resume:request.input('resume'),
          release_date:request.input('release_date'),
          genre_id:request.input('genre_id')
        })
        response.ok({message:'updated',data : affectedRows})
  }

  public async destroy({response,params}: HttpContextContract) {
    let id = params.id
        await Database.from('movies').where('id',id).delete()
        response.ok({message:'deleted'})
  }
}
