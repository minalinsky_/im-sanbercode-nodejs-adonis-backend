import {
       sapa,
       convert,
       checkScore,
       filterData
} from './lib/soal'

const myArgs = process.argv.slice(2)

switch(myArgs[0]){
    case 'sapa':
        console.log(sapa(myArgs[1]))
        break
    case 'convert':
        console.log(convert(myArgs[1],myArgs[2],myArgs[3]))
        break
    
    case 'checkScore':
        console.log(checkScore(myArgs[1]))
        break
    
    case 'filterData':
        console.log(filterData(myArgs[1]))
        break
    default :
    console.log("error")
    break
}