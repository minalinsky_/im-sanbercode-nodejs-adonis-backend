import Kelas from "./kelas";
export default class Bootcamp {

    constructor(name) {
  
      this._name = name
  
      this._classes = []
  
    }
  

    get name() {
        return this._name;
    }
    set name(x) {
        this._name = x;
    }

    get classes(){
        return this._classes
    }

    createClass(nama,level,instructor){
        const kelas = new Kelas(nama,level,instructor)
        this._classes.push(kelas)
    }

    register(kelas,newStud){
        const currentKelas = this._classes.find((current) => current._name ===kelas)
        currentKelas.addStudents(newStud)
    }
  
  }