var express = require('express');
var router = express.Router();
const VenueController = require('../controllers/venue')

/* GET users listing. */
router.get('/', VenueController.findAll);
router.post('/', VenueController.store)
router.get('/:id', VenueController.show);
router.put('/:id', VenueController.update);
router.delete('/:id', VenueController.destroy);

module.exports = router;
