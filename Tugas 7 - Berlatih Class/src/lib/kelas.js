export default class Kelas {

    constructor(name,level,instructor) {
  
      this._name = name
  
      this._students = []
      this._level = level
      this._instructor = instructor
  
    }

    get name(){
        return this._name
    }
    set name(name){
        this._name = name
    }

    get level(){
        return this._level
    }
    set level(level){
        this._level = level
    }

    get instructor(){
        return this._instructor
    }
    set instructor(instructor){
        this._instructor = instructor
    }

    get students(){
        return this._students
    }
    
    addStudents(student){
        this._students.push(student)

    }
  
  }