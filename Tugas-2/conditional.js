// soal if-else
console.log("soal if else")
console.log()
var nama = "Fajar"
var peran = "penyihir"
if(nama.length>0){
    if(peran.length>0){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        if(peran.toUpperCase() == "penyihir".toUpperCase()){
            console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")

        }
        else if(peran.toUpperCase()=="guard".toUpperCase()){
            console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
            
        }
        else if(peran.toUpperCase()=="werewolf".toUpperCase()){
            console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!")
            
        }
        else{
            console.log("Halo "+peran+" "+nama+", peranmu tidak terdaftar")

        }
    }
    else{
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
    }
}
else{
    console.log("Nama harus diisi!")
}

// soal switch-case
console.log()
console.log("soal switch-case")
console.log()

var tanggal = 21; 
var bulan = 1; 
var tahun = 1945;

var bulanKata;
if(tanggal<1 || tanggal>31){
    console.log("tanggal tidak memenuhi syarat (kurang dari 1 atau lebih dari 31)")
}
else if(bulan<1||bulan>12){
    console.log("bulan tidak memenuhi syarat (kurang dari 1 atau lebih dari 12)")

}
else if(tahun<1900||tahun>2200){
    console.log("tahun tidak memenuhi syarat (kurang dari 1900 atau lebih dari 2200)")

}
else{
    
        
    
    switch (bulan) {
        case 1:
            bulanKata="January"
            break;
        case 2:
            bulanKata="February"
            
            break;
        case 3:
            bulanKata="Maret"
            
            break;
        case 4:
            bulanKata="April"
            
            break;
        case 5:
            bulanKata="Mei"
            
            break;
        case 6:
            bulanKata="Juni"
            
            break;
        case 7:
            bulanKata="Juli"
            
            break;
        case 8:
            bulanKata="Agustus"
            
            break;
        case 9:
            bulanKata="September"
            
            break;
        case 10:
            bulanKata="Oktober"
            
            break;
        case 11:
            bulanKata="November"
            
            break;
        case 12:
            bulanKata="Desember"
            
            break;
        default:
            break;
    }
    console.log(tanggal+" "+bulanKata+" "+tahun)

}


