var express = require('express');
var router = express.Router();

// controller
const KaryawanController = require('../controllers/karyawan')
/* GET users listing. */
router.get('/', KaryawanController.findAll);
router.post('/:name/siswa', KaryawanController.addSiswa);

module.exports = router;
