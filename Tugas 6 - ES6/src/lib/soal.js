const data = [
    { name: "Ahmad", kelas: "adonis"},
    { name: "Regi", kelas: "laravel"},
    { name: "Bondra", kelas: "adonis"},
    { name: "Iqbal", kelas: "vuejs" },
    { name: "Putri", kelas: "Laravel" }
  ]
export const sapa = (nama) => `halo selamat pagi, ${nama}`

export const convert = (nama,domisili,umur) =>{
    const objConvert = {
        nama,
        domisili,
        umur
    }
    return objConvert
}

export const checkScore = (param) =>{
    let keyValues = param.split(",")
    let key = []
    let value = []
    for(var i=0;i<keyValues.length;i++){
        let keyValue=keyValues[i].split(":")
        key[i]=keyValue[0]
        value[i]=keyValue[1]
    }
    let objConvert = {
        [key[0]]:value[0],
        [key[1]]:value[1],
        [key[2]]:value[2]
    }

    return objConvert
}

export const filterData = (param) =>data.filter(kelas=>kelas.kelas.toUpperCase() == param.toUpperCase())