import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
import UpdateFieldValidator from 'App/Validators/UpdateFieldValidator'

export default class FieldsController {
  public async index({request,response,params}: HttpContextContract) {
    let venue_id = params.venue_id
    if (request.qs().name){
      let name = request.qs().name
      let fieldFiltered = await Database.from('fields').select("*").where('venue_id',venue_id).andWhere('name',name)
      return response.status(200).json({message:'successs',data:fieldFiltered})
  }
  let fields =  await Database.from('fields').where('venue_id',venue_id)

  response.status(200).json({message:'success',data:fields})
  }

  public async create({}: HttpContextContract) {}

  public async store({request,response,params}: HttpContextContract) {
    let venue_id = params.venue_id
    try{
      await request.validate(CreateFieldValidator);
      console.log("s")
      let newFieldsId = await Database.table('fields').returning('id').insert({
          name:request.input('name'),
          type:request.input('type'),
          venue_id:venue_id
      })
      response.created({status:"success add fields", newId:newFieldsId[0]})
      console.log("s")
  }
  catch(error){
      response.badRequest(error.messages)
  }
  }

  public async show({request,response,params}: HttpContextContract) {
    let venue_id = params.venue_id
    let fields = await Database.from('fields').where('venue_id',venue_id).andWhere('id',params.id).select('*').firstOrFail()
        response.ok({status:"success get data", data:fields})
        console.log("s")
  }

  public async edit({}: HttpContextContract) {
   
  }

  public async update({request,response,params}: HttpContextContract) {
    await request.validate(UpdateFieldValidator);
    let venue_id = params.venue_id
    let id = params.id
        let affectedRows = await Database.from('fields').where('id',id).update({
          name:request.input('name'),
          type:request.input('type'),
          venue_id:venue_id
        })
        response.ok({message:'updated',data : affectedRows})
  }

  public async destroy({request,response,params}: HttpContextContract) {
    let id = params.id
    let venue_id = params.venue_id
        await Database.from('fields').where('venue_id',venue_id).andWhere('id',id).delete()
        response.ok({message:'deleted'})
  }
}
