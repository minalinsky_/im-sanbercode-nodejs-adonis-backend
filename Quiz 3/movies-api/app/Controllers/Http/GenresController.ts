import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import CreateGenreValidator from 'App/Validators/CreateGenreValidator'
import UpdateGenreValidator from 'App/Validators/UpdateGenreValidator'

export default class GenresController {
  public async index({request,response}: HttpContextContract) {
    if (request.qs().name){
      let name = request.qs().name
      let genreFiltered = await Database.from('genres').select("*").where('name',name)
      return response.status(200).json({message:'success get genre',data:genreFiltered})
    }
    let genres =  await Database.from('genres').select('*')

    response.status(200).json({message:'success get genre',data:genres})
  }

  public async create({}: HttpContextContract) {}

  public async store({request,response}: HttpContextContract) {
    try{
      await request.validate(CreateGenreValidator);
      let newGenreId = await Database.table('genres').returning('id').insert({
          name:request.input('name')
      })
      response.created({status:"success add genre", newId:newGenreId[0]})
    }
    catch(error){
        response.badRequest(error.messages)
    }
  }

  public async show({response,params}: HttpContextContract) {
    let genres = await Database.from('genres').where('id',params.id).select('id','name').firstOrFail()
    let movies =  await Database.from('movies').join('genres','movies.genre_id','genres.id').select('movies.id','title','resume','release_date','genres.name as genre')
    const data = {
      ...genres,
      movies:movies
    }
        response.ok({status:"success get data", data:data})
        console.log("s")
  }

  public async edit({}: HttpContextContract) {}

  public async update({response,params,request}: HttpContextContract) {
    await request.validate(UpdateGenreValidator);
    let id = params.id
        let affectedRows = await Database.from('genres').where('id',id).update({
          name:request.input('name')
        })
        response.ok({message:'updated',data : affectedRows})
  }

  public async destroy({response,params}: HttpContextContract) {
    let id = params.id
        await Database.from('genres').where('id',id).delete()
        response.ok({message:'deleted'})
  }
  
}
