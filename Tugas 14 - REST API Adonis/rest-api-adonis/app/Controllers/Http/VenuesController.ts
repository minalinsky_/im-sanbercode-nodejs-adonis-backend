import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator';
import BookVenueValidator from 'App/Validators/BookVenueValidator';
import Database from '@ioc:Adonis/Lucid/Database';

export default class VenuesController {
  public async addVenue({request,response,params}:HttpContextContract){
      
      try{
          const payload = await request.validate(CreateVenueValidator);
          console.log(payload)
          response.status(200).json({message:"sukses tambah venue",data:payload})
      }
      catch(error){
          response.badRequest(error.messages)
      }
  }

  public async bookingVenue({request,response,params}:HttpContextContract){
      
      try{
          const payload = await request.validate(BookVenueValidator);
          console.log(payload)
          response.status(200).json({message:"sukses booking",data:payload})
      }
      catch(error){
          response.badRequest(error.messages)
      }
  }
  public async index({request,response,params}: HttpContextContract) {
    if (request.qs().name){
      let name = request.qs().name
      let venueFiltered = await Database.from('venues').select("*").where('name',name)
      return response.status(200).json({message:'success',data:venueFiltered})
  }
  let venues =  await Database.from('venues').select('*')

  response.status(200).json({message:'success',data:venues})
  }

  public async create({}: HttpContextContract) {}

  public async store({request,response,params}: HttpContextContract) {
    try{
      await request.validate(CreateVenueValidator);
      console.log("s")
      let newVenueId = await Database.table('venues').returning('id').insert({
          name:request.input('name'),
          address:request.input('address'),
          phone:request.input('phone')
      })
      response.created({status:"success add venue", newId:newVenueId[0]})
      console.log("s")
  }
  catch(error){
      response.badRequest(error.messages)
  }
  }

  public async show({request,response,params}: HttpContextContract) {
    let venues = await Database.from('venues').where('id',params.id).select('*').firstOrFail()
        response.ok({status:"success get data", data:venues})
        console.log("s")
  }

  public async edit({}: HttpContextContract) {
   
  }

  public async update({request,response,params}: HttpContextContract) {
    await request.validate(CreateVenueValidator);
    let id = params.id
        let affectedRows = await Database.from('venues').where('id',id).update({
          name:request.input('name'),
          address:request.input('address'),
          phone:request.input('phone'),
        })
        response.ok({message:'updated',data : affectedRows})
  }

  public async destroy({request,response,params}: HttpContextContract) {
    let id = params.id
        await Database.from('venues').where('id',id).delete()
        response.ok({message:'deleted'})
  }
}
