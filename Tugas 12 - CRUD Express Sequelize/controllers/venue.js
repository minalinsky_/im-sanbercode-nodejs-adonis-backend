// import model

const {Venues} = require('../models')
class VenueController {
    static async findAll(req,res){
        let venues = await Venues.findAll()

        res.status(200).json({status:'success get data', data: venues})
    }

    static async store(req,res){
        await Venues.create(req.body)
        res.status(201).json({status:'success save data'})
    }

    static async show(req,res){
        
        let id =req.params.id
        let venue = await Venues.findByPk(id)
        res.status(200).json({status:'success get data',data:venue})
    }

    static async update(req,res){
        
        let id =req.params.id
        await Venues.update(req.body,
            {
            where:{
                id:id
            }
        })
        res.status(200).json({status:'success update data'})
    }

    static async destroy(req,res){
        
        let id =req.params.id
        
        await Venues.destroy({
            where:{
                id:id
            }
        })
        res.status(200).json({status:'success delete data'})
    }
}

module.exports = VenueController