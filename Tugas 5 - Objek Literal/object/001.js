function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    for(i=0;i<arr.length;i++){
        if((arr[i][3]>thisYear)||(arr[i][3]==null)){
            var age = 'Invalid Birth Year'
        }
        else{
            // console.log(arr[i][3])
            // console.log(thisYear)
            var age = thisYear-arr[i][3]

        }
        var objVer = {
            firstName : arr[i][0],
            lastName : arr[i][1],
            gender : arr[i][2],
            age : age
            
        }
        console.log(i+1+". "+objVer.firstName+" "+objVer.lastName.trim(),objVer)
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""