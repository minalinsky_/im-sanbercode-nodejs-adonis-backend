import Bootcamp from './lib/bootcamp'
const args = process.argv.slice(2)
const command = args[0]

switch (command) {
    case "readData":
        Bootcamp.readData()
        break;
    case "register":
        Bootcamp.register(args[1])
        break;
    case "login":
        Bootcamp.login(args[1])
        break;
    case "addSiswa":
        Bootcamp.addSiswa(args[1])
        break;
    default:
        console.log("input tidak benar")
        break;
}