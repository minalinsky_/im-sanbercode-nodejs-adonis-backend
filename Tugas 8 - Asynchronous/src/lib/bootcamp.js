import Employee from "./employee";
import Trainer from "./trainer";
import Student from "./student";
import fs from 'fs'
import fspromise from 'fs/promises'
import 'core-js/stable'
import 'regenerator-runtime/runtime'

const path ="data.json";

export default class Bootcamp{
    static readData = () => {
        fs.readFile(path,(err, data)=>{
            if(err) console.log(err)
            console.log(JSON.parse(data))
        })
    }
    static register(input){
        let [name,password,role] = input.split(",")
        let newEmployee;
        console.log(role)
        console.log(role.toLowerCase()=="admin")
        if (role.toLowerCase()=="admin"){
            newEmployee = new Employee(name,password,role)
            console.log(true)
        }
        else{
            newEmployee = new Trainer(name,password,role)
            console.log(false)
        }
        fs.readFile(path,(err,data)=>{
            if(err) console.error(err)
            let dataArr = JSON.parse(data)
            dataArr.push(newEmployee)
            fs.writeFile(path,JSON.stringify(dataArr),{encoding:'utf-8'},(err)=>{
                if(err) console.log(err)
                else console.log("Berhasil register")
            })
        })
    }
    static login(input){
        let [user,password] = input.split(",")
        fspromise.readFile(path)
            .then(data => {
                let realData = JSON.parse(data)
                const loginData = realData.find((item)=> item._name == user && item._password==password)
                if(loginData == null){
                    throw "gagal login, user / password salah"
                }
                loginData._isLogin=true
                return fspromise.writeFile(path,JSON.stringify(realData))
            })
            .then(()=>console.log("berhasil login"))
            .catch(err=>{
                console.log(err)
            })
    }

    static async addSiswa(input){
        try {
            let dataRead = await fspromise.readFile(path)
            let realData = JSON.parse(dataRead)
            let [student,instructor] = input.split(",")
            const newStudent = new Student(student)
            if(!Bootcamp.checkIsLoginAdmin(realData)){
                throw "admin tidak login"
            }
            let trainerData = realData.find((item)=> item._name == instructor && item._role.toLowerCase()=="trainer")
            if (trainerData==null){
                throw "trainer tidak ada / pekerja seorang admin"
            }
            console.log(trainerData)
            trainerData._students.push(newStudent)
            // realData.splice(indexDelete,1)
            await fspromise.writeFile(path,JSON.stringify(realData))
            console.log("Berhasil add siswa")
        } catch (error) {
            console.log(error)
        }
    }

    static checkIsLoginAdmin(realData){
        let adminLogin = false
        realData.forEach(element => {
            if(element._role=="admin" && element._isLogin) {
                adminLogin= true
        }
            
        });
        return adminLogin
    }

}