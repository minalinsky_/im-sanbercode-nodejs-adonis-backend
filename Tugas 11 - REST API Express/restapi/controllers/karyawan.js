const fs = require ('fs')

class KaryawanController{
    static findAll(req,res){
        fs.readFile('data.json',(err,data)=>{
            if(err){
                res.status(400).json({"error":"error membaca data"})
            }
            else{
                let realData = JSON.parse(data)
                res.status(200).json({message:"berhasil get data", data:realData})
            }
        })

    }

    static register(req,res){
        fs.readFile('data.json',(err,data)=>{
            if(err){
                res.status(400).json({"error":"error membaca data"})
            }
            else{
                let existingData = JSON.parse(data)
                // let {users} =existingData
                let isLogin = false
                let student =[]
                let newUser
                let {name,password,role}=req.body
                if(role.toLowerCase() === "trainer"){
                    newUser = {name,password,role,isLogin,student}
                }
                else{
                    newUser = {name,password,role,isLogin}
                }
                
                console.log(existingData)
                existingData.push(newUser)
                let newData = existingData
                console.log(newUser)
                fs.writeFile('data.json',JSON.stringify(newData),(err)=>{
                    if (err){
                        res.status(400).json({error:"error Menyimpan Data"})
                    }
                    else{
                        res.status(200).json({message:"berhasil register"})
                    }
                })
                // console.log(req.body)
                // res.send("ok")
            }
        })
    }

    static login(req,res){
        fs.readFile('data.json',(err,data)=>{
            if(err){
                res.status(400).json({"error":"error membaca data"})
            }
            else{
                let existingData = JSON.parse(data)
                let {name,password}=req.body
                console.log(existingData)
                const loginData = existingData.find((item)=> item.name == name && item.password==password)
                if(loginData == null){
                    res.status(404).json({error:"data tidak ada"})
                }
                else{
                    loginData.isLogin=true
                    fs.writeFile('data.json',JSON.stringify(existingData),(err)=>{
                        if (err){
                            res.status(400).json({error:"gagal login"})
                        }
                        else{
                            res.status(200).json({message:"berhasil login"})
                        }
                    })
                }
            }
        })
    }

    static addSiswa(req,res){
        fs.readFile('data.json',(err,data)=>{
            if(err){
                res.status(400).json({"error":"error membaca data"})
            }
            else{
                let existingData = JSON.parse(data)
                // let {name,class}=req.body
                console.log(existingData)
                console.log(req.body)
                console.log(req.body)
                let instructor = req.params.name


                if(!KaryawanController.checkIsLoginAdmin(existingData)){
                    res.status(403).json( "admin tidak login")
                }
                else{
                    let trainerData = existingData.find((item)=> item.name == instructor && item.role.toLowerCase()=="trainer")
                    if (trainerData==null){
                        res.status(404).json( "trainer tidak ada / pekerja seorang admin")
                    }
                    else{
                        console.log(trainerData)
                        trainerData.student.push(req.body)
                        fs.writeFile('data.json',JSON.stringify(existingData),(err)=>{
                            if (err){
                                res.status(400).json({error:"gagal tambah student",err:err})
                            }
                            else{
                                res.status(200).json({message:"berhasil tambah student"})
                            }
                        })
                    }
                }
                
                    
                
                // console.log(req.body)
                // res.send("ok")
            }
        })
    }



    static checkIsLoginAdmin(realData){
        let adminLogin = false
        realData.forEach(element => {
            if(element.role=="admin" && element.isLogin) {
                adminLogin= true
        }
            
        });
        return adminLogin
    }
    
}

module.exports = KaryawanController