import Employee from "./employee"
export default class Trainer extends Employee{
    constructor(name,password,role){
        super(name,password,role)
        this._students = []
    }

    
    get students(){
        return this._students
    }
    
    addStudents(student){
        this._students.push(student)

    }

}