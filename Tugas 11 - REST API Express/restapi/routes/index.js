var express = require('express');
var router = express.Router();

// controller
const KaryawanController = require('../controllers/karyawan')
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post('/register', KaryawanController.register);
router.post('/login', KaryawanController.login);

module.exports = router;
