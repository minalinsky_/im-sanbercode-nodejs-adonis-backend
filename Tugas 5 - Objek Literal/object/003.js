function nilaiTertinggi(siswa) {
    // Code disini
    var output = []
    for(i=0;i<siswa.length;i++){
        // console.log(siswa[i])
        if(output.hasOwnProperty(siswa[i].class)){
            if(output[siswa[i].class].score<siswa[i].score){
                output[siswa[i].class].score=siswa[i].score
                output[siswa[i].class].name=siswa[i].name
            }
            else if((output[siswa[i].class].score==siswa[i].score)){
                output[siswa[i].class]=[
                    output[siswa[i].class],
                    {
                        name:siswa[i].name,
                        score:siswa[i].score
                    }
                ]
                
            }
        }
        else{
            output ={...output,
                [siswa[i].class]:{
                    name:siswa[i].name,
                    score:siswa[i].score
                }
            }
        }
            
    }
    return output;
  }
  
  // TEST CASE
  console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]));
  
  // OUTPUT:
  
  // {
  //   adonis: { name: 'Asep', score: 90 },
  //   vuejs: { name: 'Ahmad', score: 85 },
  //   reactjs: { name: 'Afrida', score: 78}
  // }
  
  
  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]));
  
  // {
  //   adonis: { name: 'Bondra', score: 100 },
  //   laravel: { name: 'Putri', score: 76 },
  //   vuejs: { name: 'Hilmy', score: 80 }
  // }