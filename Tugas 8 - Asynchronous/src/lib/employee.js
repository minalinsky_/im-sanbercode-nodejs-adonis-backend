export default class Employee{
    constructor(name,password,role){
        this._name=name
        this._password=password
        this._role=role
        this._isLogin = false
    }

    get name(){
        return this._name
    }
    set name(newName){
        this._name=newName
    }

    get role(){
        return this._role
    }
    set role(role){
        this._role=role
    }

    get password(){
        return this._password
    }
    set password(password){
        this._password=password
    }

    get isLogin(){
        return this._isLogin
    }
    set isLogin(islogin){
        this._isLogin=islogin
    }
    logIn(status){
        this._isLogin=status
    }
    
}
